from django.shortcuts import render, redirect, reverse, get_object_or_404
from django.views.generic import CreateView, UpdateView, TemplateView, View, FormView
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.contrib import messages

from .models import Usuario, ArchivosPdf
from .forms import UsuarioForm, CargueArchivoForm

# Create your views here.

class HomeView(LoginRequiredMixin, TemplateView):
    """
    autor: Duvernei
    fecha: 2021-11
    descripcion: Pagina de inicio

    """
    template_name = 'home/index.html'


class CrearUsuarioView(LoginRequiredMixin, CreateView):
    """
    autor: Duvernei
    fecha: 2021-11
    descripcion: Pagina de crear usuario
    """
    model = Usuario
    form_class = UsuarioForm
    template_name = 'home/gestion_usuarios.html'

    def dispatch(self, request, *args, **kwargs):
        usuario = get_object_or_404(Usuario, id=request.user.id)
        if usuario.perfil.tipo != 'ADMINISTRADOR':
            messages.error(self.request, "Usted no tiene permisos para gestionar usuarios")
            return redirect(reverse('inicio', kwargs={}))
        return super(CrearUsuarioView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['list_users'] = Usuario.objects.all()
        return context

    def form_valid(self, form):
        usuario = form.save(commit=False)
        usuario.set_password(usuario.password)
        messages.success(self.request, "Usuario registrado correctamente")
        return super(CrearUsuarioView, self).form_valid(form)

    def get_success_url(self):
        return reverse('home:usuarios', kwargs={})


class EditarUsuarioView(LoginRequiredMixin, UpdateView):
    """
    autor: Duvernei
    fecha: 2021-11
    descripcion: Pagina de editar usuario
    """
    model = Usuario
    form_class = UsuarioForm
    template_name = 'home/gestion_usuarios.html'

    def dispatch(self, request, *args, **kwargs):
        usuario = get_object_or_404(Usuario, id=request.user.id)
        if usuario.perfil.tipo != 'ADMINISTRADOR':
            messages.error(self.request, "Usted no tiene permisos para gestionar usuarios")
            return redirect(reverse('inicio', kwargs={}))
        return super(EditarUsuarioView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['list_users'] = Usuario.objects.all()
        return context

    def form_valid(self, form):
        usuario = form.save(commit=False)
        if 'password' in form.changed_data:
            usuario.set_password(usuario.password)
        messages.success(self.request, "Usuario actualizado correctamente")
        return super(EditarUsuarioView, self).form_valid(form)
    
    def get_success_url(self):
        return reverse('home:usuarios', kwargs={})


class EliminarUsuarioView(LoginRequiredMixin, View):
    """
    autor: Duvernei
    fecha: 2021-11
    descripcion: Pagina de eliminar usuario
    """
    def dispatch(self, request, *args, **kwargs):
        usuario = get_object_or_404(Usuario, id=request.user.id)
        if usuario.perfil.tipo != 'ADMINISTRADOR':
            messages.error(self.request, "Usted no tiene permisos para gestionar usuarios")
            return redirect(reverse('inicio', kwargs={}))
        return super(EliminarUsuarioView, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        usuario = get_object_or_404(Usuario, id=self.kwargs['pk'])
        usuario.delete()
        return redirect(reverse('home:usuarios', kwargs={}))


class CargarPdfView(LoginRequiredMixin, FormView):
    """
    autor: Duvernei
    fecha: 2021-11
    descripcion: Pagina de carga de pdf

    """
    #permission_required = 'home.add_archivospdf'
    template_name = 'home/cargar_pdf.html'
    form_class = CargueArchivoForm

    def form_valid(self, form):
        archivospdf = form.save(commit=False)
        archivospdf.usuario = self.request.user
        archivospdf.save()
        messages.success(self.request, "Archivos pdf cargados correctamente")
        return redirect(reverse('home:cargar_pdf', kwargs={}))

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        archivos = ArchivosPdf.objects.filter(usuario=self.request.user)
        if self.request.user.perfil.tipo == 'ADMINISTRADOR':
            archivos = ArchivosPdf.objects.all()
        context['list_files'] = archivos
        return context
