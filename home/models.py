from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils.encoding import smart_str

from datetime import datetime
import os

# Create your models here.

class PerfilUsuario(models.Model):
    tipo = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return "{} ".format(str(self.tipo))


class Usuario(AbstractUser):
    #CLIENTE = 1
    #ADMINISTRADOR = 2
    #PERFILES = ((CLIENTE, 'CLIENTE'), (ADMINISTRADOR, 'ADMINISTRADOR'))
    #perfil = models.PositiveIntegerField(choices=PERFILES, default=CLIENTE)

    perfil = models.ForeignKey(PerfilUsuario, on_delete=models.CASCADE)

    def __str__(self):
        return "{} ({})".format(str(self.username), str(self.perfil))


class ArchivosPdf(models.Model):

    def generate_filename(self, filename):
        fecha = datetime.now()
        path = "documentos_pdf/%s/%s" % (fecha.date(), smart_str(filename))
        return path

    usuario = models.ForeignKey(Usuario, on_delete=models.CASCADE, null=True, blank=True)
    pdf_uno = models.FileField(null=True, blank=True, upload_to=generate_filename, max_length=500)
    pdf_dos = models.FileField(null=True, blank=True, upload_to=generate_filename, max_length=500)

    def get_name_pdf_uno(self):
        return os.path.basename(self.pdf_uno.name)

    def get_name_pdf_dos(self):
        return os.path.basename(self.pdf_dos.name)
