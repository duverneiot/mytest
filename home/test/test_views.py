from django.test import TestCase

from django.core.files.uploadedfile import SimpleUploadedFile
from django.shortcuts import reverse

from home.models import Usuario


class CrearUsuarioViewTestCase(TestCase):
    fixtures = ['data_test_home']

    def setUp(self):
        self.usuario_admin = Usuario.objects.get(username='admin1')
        self.usuario_admin.set_password('12345')
        self.usuario_admin.save()
        self.usuario_cliente = Usuario.objects.get(username='cliente1')
        self.usuario_cliente.set_password('54321')
        self.usuario_cliente.save()
        self.url = reverse('home:usuarios')

    def tearDown(self):
        del self.usuario_admin
        del self.usuario_cliente
        del self.url

    def test_get_200(self):
        """
        Autor: Duvernei
        Fecha: 2021-12
        método para testear el status 200 de la vista CrearUsuarioView metodo get
        """
        # Logueo de un usuario
        login = self.client.login(username='admin1', password='12345')
        self.assertTrue(login)

        # Intento de acceso por un usuario con permisos
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)

    def test_get_302(self):
        """
        Autor: Duvernei
        Fecha: 2021-12
        método para testear el status 302 de la vista CrearUsuarioView metodo get
        """
        # Logueo de un usuario
        login = self.client.login(username='cliente1', password='54321')
        self.assertTrue(login)

        # Intento de acceso por un usuario con permisos
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 302)

    def test_post_302(self):
        """
        Autor: Duvernei
        Fecha: 2021-12
        método para testear el status 302 de la vista CrearUsuarioView metodo post
        """
        # Logueo de un usuario
        login = self.client.login(username='admin1', password='12345')
        self.assertTrue(login)

        data = {
            'username': 'cliente2',
            'password': 55555,
            'email': 'test2@test.com',
            'perfil': 2
        }

        # se envian los datos del primer formulario
        response = self.client.post(self.url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_post_200(self):
        """
        Autor: Duvernei
        Fecha: 2021-12
        método para testear el status 200 de la vista CrearUsuarioView metodo post
        """
        # Logueo de un usuario
        login = self.client.login(username='admin1', password='12345')
        self.assertTrue(login)

        data = {
            'username': 'cliente1',
            'password': 55555,
            'email': 'test2@test.com',
            'perfil': 100
        }

        # se envian los datos del primer formulario
        response = self.client.post(self.url, data=data)
        self.assertEqual(response.status_code, 200)


class EditarUsuarioViewTestCase(TestCase):
    fixtures = ['data_test_home']

    def setUp(self):
        self.usuario_admin = Usuario.objects.get(username='admin1')
        self.usuario_admin.set_password('12345')
        self.usuario_admin.save()
        self.usuario_cliente = Usuario.objects.get(username='cliente1')
        self.usuario_cliente.set_password('54321')
        self.usuario_cliente.save()
        self.url = reverse('home:editar_usuario', kwargs={'pk': 2})

    def tearDown(self):
        del self.usuario_admin
        del self.usuario_cliente
        del self.url

    def test_get_200(self):
        """
        Autor: Duvernei
        Fecha: 2021-12
        método para testear el status 200 de la vista EditarUsuarioView metodo get
        """
        # Logueo de un usuario
        login = self.client.login(username='admin1', password='12345')
        self.assertTrue(login)

        # Intento de acceso por un usuario con permisos
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)

    def test_get_302(self):
        """
        Autor: Duvernei
        Fecha: 2021-12
        método para testear el status 302 de la vista EditarUsuarioView metodo get
        """
        # Logueo de un usuario
        login = self.client.login(username='cliente1', password='54321')
        self.assertTrue(login)

        # Intento de acceso por un usuario con permisos
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 302)

    def test_post_302(self):
        """
        Autor: Duvernei
        Fecha: 2021-12
        método para testear el status 302 de la vista EditarUsuarioView metodo post
        """
        # Logueo de un usuario
        login = self.client.login(username='admin1', password='12345')
        self.assertTrue(login)

        data = {
            'username': 'cliente2',
            'password': 55555,
            'email': 'test2@test.com',
            'perfil': 2
        }

        # se envian los datos del formulario
        response = self.client.post(self.url, data=data)
        self.assertEqual(response.status_code, 302)


class EliminarUsuarioViewTestCase(TestCase):
    fixtures = ['data_test_home']

    def setUp(self):
        self.usuario_admin = Usuario.objects.get(username='admin1')
        self.usuario_admin.set_password('12345')
        self.usuario_admin.save()
        self.usuario_cliente = Usuario.objects.get(username='cliente1')
        self.usuario_cliente.set_password('54321')
        self.usuario_cliente.save()
        self.url = reverse('home:eliminar_usuario', kwargs={'pk': 2})

    def tearDown(self):
        del self.usuario_admin
        del self.usuario_cliente
        del self.url

    def test_get_302_ok(self):
        """
        Autor: Duvernei
        Fecha: 2021-12
        método para testear el status 302 de la vista EliminarUsuarioView metodo get
        """
        # Logueo de un usuario
        login = self.client.login(username='admin1', password='12345')
        self.assertTrue(login)

        # Intento de acceso por un usuario con permisos
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 302)

    def test_get_302(self):
        """
        Autor: Duvernei
        Fecha: 2021-12
        método para testear el status 302 de la vista EliminarUsuarioView metodo get
        """
        # Logueo de un usuario
        login = self.client.login(username='cliente1', password='54321')
        self.assertTrue(login)

        # Intento de acceso por un usuario con permisos
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 302)


class CargarPdfViewTestCase(TestCase):
    fixtures = ['data_test_home']

    def setUp(self):
        self.usuario_admin = Usuario.objects.get(username='admin1')
        self.usuario_admin.set_password('12345')
        self.usuario_admin.save()
        self.usuario_cliente = Usuario.objects.get(username='cliente1')
        self.usuario_cliente.set_password('54321')
        self.usuario_cliente.save()
        self.url = reverse('home:cargar_pdf')

    def tearDown(self):
        del self.usuario_admin
        del self.usuario_cliente
        del self.url

    def test_get_200(self):
        """
        Autor: Duvernei
        Fecha: 2021-12
        método para testear el status 200 de la vista CargarPdfView metodo get
        """
        # Logueo de un usuario
        login = self.client.login(username='admin1', password='12345')
        self.assertTrue(login)

        # Intento de acceso por un usuario con permisos
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)

    def test_get_302(self):
        """
        Autor: Duvernei
        Fecha: 2021-12
        método para testear el status 302 de la vista CargarPdfView metodo get
        """
        # Logueo de un usuario
        login = self.client.login(username='cliente1', password='54321')
        self.assertTrue(login)

        archivo_pdf1 = SimpleUploadedFile(name='prueba1.pdf',  content=open('static/assets/pdf/prueba.pdf', 'rb').read(),
                                              content_type='application/pdf')

        archivo_pdf2 = SimpleUploadedFile(name='prueba2.pdf',  content=open('static/assets/pdf/prueba.pdf', 'rb').read(),
                                              content_type='application/pdf')

        data = {
            'pdf_uno': archivo_pdf1,
            'pdf_dos': archivo_pdf2
        }

        # se envian los datos del formulario
        response = self.client.post(self.url, data=data)
        self.assertEqual(response.status_code, 302)
