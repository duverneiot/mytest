from home.models import *

perfil_admin = PerfilUsuario.objects.create(tipo="ADMINISTRADOR")
perfil_cliente = PerfilUsuario.objects.create(tipo="CLIENTE")

u = Usuario.objects.create(username="admin1", perfil=perfil_admin)
u.set_password("12345")
u.save()

u = Usuario.objects.create(username="cliente1", perfil=perfil_cliente)
u.set_password("54321")
u.save()
