from django import forms
from home.models import Usuario, ArchivosPdf

from django.core.validators import FileExtensionValidator


class UsuarioForm(forms.ModelForm):
    """
    Autor: Duvernei
    Fecha: 2021-11
    Formulario para creacion/actualizacion de usuario
    """
    def __init__(self, *args, **kwargs):
        super(UsuarioForm, self).__init__(*args, **kwargs)
        self.fields['password'].widget = forms.PasswordInput(render_value=True)

    class Meta:
        model = Usuario
        fields = ('username', 'password', 'email', 'perfil')


class CargueArchivoForm(forms.ModelForm):
    """
    Autor: Duvernei
    Fecha: 2021-11
    Formulario para el cargue de archivos pdf
    """
    def __init__(self, *args, **kwargs):
        super(CargueArchivoForm, self).__init__(*args, **kwargs)
        self.fields['pdf_uno'] = forms.FileField(validators=[
            FileExtensionValidator(allowed_extensions=['pdf'],
                                   message="La extensión del archivo '%(extension)s' no esta permitida. "
                                           "Las extensiones permitida son: '%(allowed_extensions)s'.")])
        self.fields['pdf_dos'] = forms.FileField(validators=[
            FileExtensionValidator(allowed_extensions=['pdf'],
                                   message="La extensión del archivo '%(extension)s' no esta permitida. "
                                           "Las extensiones permitida son: '%(allowed_extensions)s'.")])

    class Meta:
        model = ArchivosPdf
        fields = ('pdf_uno', 'pdf_dos')