from django.urls import include, path, re_path
from . import views

#from django.conf.urls import url #, include

app_name = 'home'

urlpatterns = [
    path('index', views.HomeView.as_view(), name='index'),
    path('usuarios', views.CrearUsuarioView.as_view(), name='usuarios'),
    re_path(r'^editar-usuario/(?P<pk>\d+)$', views.EditarUsuarioView.as_view(), name='editar_usuario'),
    re_path(r'^eliminar-usuario/(?P<pk>\d+)$', views.EliminarUsuarioView.as_view(), name='eliminar_usuario'),
    path('cargar-pdf', views.CargarPdfView.as_view(), name='cargar_pdf'),
]
